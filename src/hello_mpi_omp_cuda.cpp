// MPI include
#include <mpi.h>
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/unistd.h>
#include <iostream>

using std::cout;
using std::cerr;
using std::endl;

#include "hello_mpi_omp_cuda.h"

int main(int argc, char *argv[])
{

  int mpiRank, mpiSize;
  char hostname[128];
  int nthreads, tid, cpuid;
  int igpu,i, j=0;
  int k =0;
  
  MPI_Init(&argc,&argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank);
  MPI_Comm_size(MPI_COMM_WORLD, &mpiSize);

  gethostname(hostname, sizeof hostname);
  int DGPU;
  int nb_GPU;
  nb_GPU=hello_countGPU();

#pragma omp parallel
  {
    nthreads = omp_get_num_threads();
  }
  
  if ( mpiRank== 0)  
    {
      printf("Run executed using %d MPI processes, with %d threads per process on %d GPUs\n", mpiSize, nthreads,nb_GPU);

      if (mpiSize < nb_GPU || (mpiSize%nb_GPU) != 0)
	{
	  printf("nb of mpi tasks must be >= nb of GPUs \n");
	  printf("or \n");
	  printf("nb of mpi tasks must be divisible by nb of GPUs \n");
	  return 0;
	}
    }
      for(igpu = 0; igpu < nb_GPU; igpu++)   
	{
	  for(i = k; i < (igpu+1)*(mpiSize/nb_GPU); i++) 
	    {
	      MPI_Barrier(MPI_COMM_WORLD);
	      if (i == mpiRank) 
		{
		  hello_displayGPU(igpu, &DGPU);
		  printf("%s: MPI n° %d -> cpuid %d -> on GPU busId n°%d\n",hostname, mpiRank,sched_getcpu(),DGPU);
#pragma omp parallel private(tid, nthreads, cpuid) shared(i)
		  {
		    tid=omp_get_thread_num();
		    nthreads = omp_get_num_threads();
		    cpuid = sched_getcpu();
		    
		    while(j < tid)
		  {
#pragma omp flush(j)
		  }
		    printf("\t thread n° %d -> cpuid %d on MPI n° %d on %s\n", tid, cpuid, mpiRank,hostname);
		    j++;
#pragma omp flush(j)
		  }
		}
	    }
	  k=k+(mpiSize/nb_GPU);
	}
      MPI_Finalize();
}
