#include <iostream>
using std::cerr;
using std::endl;

#include "hello_mpi_omp_cuda.h"

// CUDA runtime
#include <cuda_runtime.h>


// Error handling macro
#define CUDA_CHECK(call) \
    if((call) != cudaSuccess) { \
        cudaError_t err = cudaGetLastError(); \
        cerr << "CUDA error calling \""#call"\", code is " << err << endl; \
        my_abort(err); }

int hello_countGPU()
{
  int deviceCount = 0;
  cudaError_t error_id = cudaGetDeviceCount(&deviceCount);
  if (error_id != cudaSuccess) 
    {
      printf("cudaGetDeviceCount returned %d\n-> %s\n",
	     static_cast<int>(error_id), cudaGetErrorString(error_id));
      printf("Result = FAIL\n");
      exit(EXIT_FAILURE);
    }
  return deviceCount;
}

int hello_displayGPU(int gpuid, int *DGPU)
{ 
  
  cudaDeviceProp deviceProp;
  cudaGetDeviceProperties(&deviceProp, gpuid);
  *DGPU=deviceProp.pciBusID;
  return 0;  
}



