#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/unistd.h>

int main(int argc,char**argv)
{
  char hostname[128];
  int nthreads, cpuid, tid;  
	int i=0;

  gethostname(hostname, sizeof hostname);

#pragma omp parallel private(tid, nthreads, cpuid) shared(i)
  {
    tid=omp_get_thread_num();
    nthreads = omp_get_num_threads();
    cpuid = sched_getcpu();

		if(tid == 0)
			printf("Run executed using %d threads \n", nthreads);

		while(i < tid){
			#pragma omp flush(i)
		}  
   	printf("on host %s, thread n° %d/%d, for cpuid %d \n", hostname, tid, nthreads, cpuid);
		i++;
			#pragma omp flush(i)
	}

}

