#include <mpi.h>
#include <stdlib.h>
#include <sys/unistd.h>
#include <stdio.h>
#include <string.h>

int main(int argc,char**argv)
{
  int mpiRank, mpiSize, i;
  char hostname[128];
  int nthreads, tid, cpuid;  

  MPI_Init(&argc,&argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank);
  MPI_Comm_size(MPI_COMM_WORLD, &mpiSize);

  gethostname(hostname, sizeof hostname);

  if ( mpiRank== 0) 
		printf("Run executed using %d MPI processes \n", mpiSize);

	for(i = 0; i < mpiSize; i++) {
    MPI_Barrier(MPI_COMM_WORLD);
    if (i == mpiRank) {
  		printf("%s: MPI n° %d -> cpuid %d \n",hostname, mpiRank,sched_getcpu());
    }
	}

  MPI_Finalize();
}
