#!/bin/bash
#SBATCH -J comp_hello_world
#SBATCH -C SKL224
#SBATCH -t 00:30:00
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=1
#SBATCH --threads-per-core=1
#SBATCH --gres=gpu:1
#SBATCH --mem=100GB

module purge
module load intel/18.1 intelmpi/2018.1.163 gcc/6.2.0 cuda/10.1
make -f Makefile_mpi_omp_cuda
