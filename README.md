# Project Title

Hello World hybrid (MP and/or OpenMP) for process binding verification

## Getting Started

Clone the repo:

```
git clone https://dci-gitlab.cines.fr/hautreux/hello_world.git
```

and enter the floder:
```
cd hello_world
```

### Prerequisites

Intel compiler and MPI library

```
module load intel
module load intelmpi
```

You may also want to use Slurm for running the tests.

### Installing

Compile it using make:

```
make
```
## Running the tests

Tests are available in test\_scripts directory

You can run them using Slurm:
```
cd test_scripts/occigen/mpi/
sbatch hsw24.sh
```
Then you can check the created output file to verify the binding.

## Authors

* **CINES support team** 


## How to contribute

1. Create an issue
2. Create a branch from the "dev" branch for/from this issue
3. Fix the issue
4. Then, ask a merge request to dev branch


## License

TBD

## Acknowledgments

* TBD

