CC=icc
MPICC=mpicc
LIB_OMP=-qopenmp
# Specify the compiler flags
# Optimized
CFLAGS = -O3 -xhost 
# Debugging : -g Save symbols for debuggers, -traceback Extra information
# CFLAGS = -O0 -g -traceback
# gprof profiling
# CFLAGS = -pg -O 

SRC_DIR=src

all: hello_mpi hello_omp hello_hybrid

hello_mpi: $(SRC_DIR)/hello_mpi.c 
	$(MPICC) $(CFLAGS) -o bin/$@ $^ 

hello_omp: $(SRC_DIR)/hello_omp.c 
	$(CC) $(CFLAGS) -o bin/$@ $^ $(LIB_OMP) 

hello_hybrid: $(SRC_DIR)/hello_hybrid.c 
	$(MPICC) $(CFLAGS) -o bin/$@ $^ $(LIB_OMP) 

clean:
	rm bin/*
