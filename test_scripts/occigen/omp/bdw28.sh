#!/bin/bash
#SBATCH -J bdw28_omp 
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=28
#SBATCH --time=0:40:00
#SBATCH -C BDW28
#SBATCH --exclusive
#SBATCH --mem=50GB
#SBATCH --output bdw_omp.output.slurm 

set -e

#Make sure that OMP_NUM_THREADS = cpus-per-task * KMP_HW_SUBSET
export KMP_HW_SUBSET=1T
export OMP_NUM_THREADS=28
export KMP_AFFINITY=verbose,compact,1,0,granularity=fine

module load intel 

ulimit -s unlimited

rm -f *.out

srun ../../../bin/hello_omp


