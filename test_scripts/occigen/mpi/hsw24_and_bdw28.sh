#!/bin/bash
#SBATCH -J mpi_hello
#SBATCH --ntasks=128
#SBATCH --threads-per-core=1
#SBATCH --time=0:30:00

#EXPLICIT AND
#SBATCH --constraint="[HSW24*3&BDW28*2]"

#SBATCH --exclusive
#SBATCH --mem=50GB
#SBATCH --output h_and_b_mpi.output.slurm 

set -e

#####Intelmpi placement auto 
# module load intel/18.1 intelmpi/2018.1.163 
# export I_MPI_DOMAIN=auto
# export I_MPI_PIN_RESPECT_CPUSET=0
# ulimit -s unlimited
# srun ../../../bin/hello_mpi

####Openmpi placement auto 
module load intel/18.1 openmpi/intel/2.0.2
ulimit -s unlimited
srun ../../../bin/hello_mpi
