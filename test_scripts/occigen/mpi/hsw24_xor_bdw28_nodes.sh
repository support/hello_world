#!/bin/bash
#SBATCH -J mpi_hello
#SBATCH -N 2
#SBATCH --threads-per-core=1
#SBATCH --time=0:40:00

#EXCLUSIVE OR
#SBATCH -C [HSW24|BDW28]

#SBATCH --exclusive
#SBATCH --mem=50GB
#SBATCH --output hsw_xor_n_mpi.output.slurm.%J

set -e

export NCPUS=$(($SLURM_CPUS_ON_NODE*$SLURM_NNODES))

#####Intelmpi placement auto 
# module load intel/18.1 intelmpi/2018.1.163 
# export I_MPI_DOMAIN=auto
# export I_MPI_PIN_RESPECT_CPUSET=0
# ulimit -s unlimited
# srun -n $NCPUS ../../../bin/hello_mpi

####Openmpi placement auto 
module load intel/18.1 openmpi/intel/2.0.2
ulimit -s unlimited
srun -n $NCPUS ../../../bin/hello_mpi
