#!/bin/bash
#SBATCH -J bdw28_mpi
#SBATCH --nodes=6
#SBATCH --ntasks=168
#SBATCH --ntasks-per-node=28
#SBATCH --time=0:40:00
#SBATCH -C BDW28 
#SBATCH --exclusive
#SBATCH --mem=50GB
#SBATCH --output bdw_mpi.output.slurm 

set -e

#####Intelmpi placement auto 
# module load intel/18.1 intelmpi/2018.1.163 
# export I_MPI_DOMAIN=auto
# export I_MPI_PIN_RESPECT_CPUSET=0
# ulimit -s unlimited
# srun ../../../bin/hello_mpi

#####Intelmpi avec placement pour mpirun
# module load intel/18.1 intelmpi/2018.1.163
# export SLURM_CPU_BIND=NONE
# export I_MPI_PIN=1
# export I_MPI_PIN_PROCESSOR_LIST=0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27
# ulimit -s unlimited
# mpirun ../../../bin/hello_mpi


####Openmpi placement auto 
module load intel/18.1 openmpi/intel/2.0.2 
ulimit -s unlimited
srun ../../../bin/hello_mpi




