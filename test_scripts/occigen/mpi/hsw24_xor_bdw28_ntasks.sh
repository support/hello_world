#!/bin/bash
#SBATCH -J mpi_hello
#SBATCH --ntasks=168
#SBATCH --threads-per-core=1
#SBATCH --time=0:40:00

#EXCLUSIVE OR
#SBATCH -C [HSW24|BDW28]

#SBATCH --exclusive
#SBATCH --mem=50GB
#SBATCH --output hsw_mpi.output.slurm.%J

set -e

export I_MPI_DOMAIN=auto
export I_MPI_PIN_RESPECT_CPUSET=0
export I_MPI_DEBUG=4


module load intel intelmpi

ulimit -s unlimited

srun ../../../bin/hello_mpi


