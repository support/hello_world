#!/bin/bash
#SBATCH -J hsw24_mpi
#SBATCH --nodes=7
#SBATCH --ntasks=168
#SBATCH --ntasks-per-node=24
#SBATCH --cpus-per-task=1
#SBATCH --time=0:30:00
#SBATCH -C HSW24 
#SBATCH --exclusive
#SBATCH --mem=50GB
#SBATCH --output hsw_mpi.output.slurm 

set -e

#####Intelmpi placement auto 
# module load intel/18.1 intelmpi/2018.1.163 
# export I_MPI_DOMAIN=auto
# export I_MPI_PIN_RESPECT_CPUSET=0
# ulimit -s unlimited
# srun ../../../bin/hello_mpi

####Openmpi placement auto 
module load intel/18.1 openmpi/intel/2.0.2
ulimit -s unlimited
srun ../../../bin/hello_mpi
