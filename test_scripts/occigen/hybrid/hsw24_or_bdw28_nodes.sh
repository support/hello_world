#!/bin/bash
#SBATCH -J or_hybrid
#SBATCH -N 3
#SBATCH --threads-per-core=1
#SBATCH --cpus-per-task=4
#SBATCH --time=0:30:00
#SBATCH --exclusive
#SBATCH --mem=50GB
#SBATCH --output or_hybrid.output.slurm.%J

#OR
#SBATCH -C HSW24|BDW28
set -e


#####Intelmpi
# module load intel intelmpi
# export I_MPI_DOMAIN=auto
# export I_MPI_PIN_RESPECT_CPUSET=0
# #Make sure that OMP_NUM_THREADS = cpus-per-task * KMP_HW_SUBSET
# export KMP_HW_SUBSET=1T
# export OMP_NUM_THREADS=4
# export KMP_AFFINITY=verbose,compact,1,0,granularity=fine
# export NCPUS=$((($SLURM_CPUS_ON_NODE/$OMP_NUM_THREADS)*$SLURM_NNODES))
# ulimit -s unlimited
# srun -n $NCPUS ../../../bin/hello_hybrid

#####Openmpi
module load intel/18.1 openmpi/intel/2.0.2
#Make sure that OMP_NUM_THREADS = cpus-per-task * KMP_HW_SUBSET
export KMP_HW_SUBSET=1T
export OMP_NUM_THREADS=4
export KMP_AFFINITY=verbose,compact,1,0,granularity=fine
export NCPUS=$((($SLURM_CPUS_ON_NODE/$OMP_NUM_THREADS)*$SLURM_NNODES))
ulimit -s unlimited
srun -n $NCPUS ../../../bin/hello_hybrid

