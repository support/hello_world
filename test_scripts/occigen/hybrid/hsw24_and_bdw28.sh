#!/bin/bash
#SBATCH -J and_hybrid
#SBATCH --threads-per-core=1
#SBATCH --cpus-per-task=12
#SBATCH --time=0:40:00
#SBATCH --exclusive
#SBATCH --mem=50GB
#SBATCH --output and_hybrid.output.slurm 

#EXPLICIT AND
#SBATCH --constraint="[HSW24*4&BDW28*3]"

set -e



#####Intelmpi
# module load intel intelmpi
# export I_MPI_DOMAIN=auto
# export I_MPI_PIN_RESPECT_CPUSET=0
# #Make sure that OMP_NUM_THREADS = cpus-per-task * KMP_HW_SUBSET
# export KMP_HW_SUBSET=1T
# export OMP_NUM_THREADS=12
# export KMP_AFFINITY=verbose,compact,1,0,granularity=fine
# ulimit -s unlimited
# srun ../../../bin/hello_hybrid

#####Openmpi
module load intel/18.1 openmpi/intel/2.0.2
#Make sure that OMP_NUM_THREADS = cpus-per-task * KMP_HW_SUBSET
export KMP_HW_SUBSET=1T
export OMP_NUM_THREADS=12
export KMP_AFFINITY=verbose,compact,1,0,granularity=fine
ulimit -s unlimited
srun ../../../bin/hello_hybrid

