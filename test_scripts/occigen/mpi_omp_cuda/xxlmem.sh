#!/bin/bash
#SBATCH -J hello_world
#SBATCH -C XXLMEM
#SBATCH -t 00:30:00
#SBATCH --nodes=1
#SBATCH --ntasks=8
#SBATCH --ntasks-per-node=8
#SBATCH --cpus-per-task=7
#SBATCH --threads-per-core=1
#SBATCH --gres=gpu:2
#SBATCH --mem=100GB

module purge
module load intel/18.1 intelmpi/2018.1.163 gcc/6.2.0 cuda/10.1

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export KMP_HW_SUBSET=1T
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export KMP_AFFINITY=compact,1,0,granularity=fine
srun -n $SLURM_NTASKS ./hello_mpi_omp_cuda

