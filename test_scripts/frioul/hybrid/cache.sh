#!/bin/bash
#SBATCH -J hybrid
#SBATCH --nodes=7
#SBATCH --ntasks=28
#SBATCH --ntasks-per-node=4
#SBATCH --cpus-per-task=17
#SBATCH --time=0:40:00
#SBATCH -C quad,cache
#SBATCH --exclusive
#SBATCH --mem=50GB
#SBATCH --output hybrid.output.slurm 

set -e

export I_MPI_DOMAIN=auto
export I_MPI_PIN_RESPECT_CPUSET=0

#Make sure that OMP_NUM_THREADS = cpus-per-task * KMP_HW_SUBSET
export KMP_HW_SUBSET=1T
export OMP_NUM_THREADS=17
export KMP_AFFINITY=compact,1,0,granularity=fine

module load intel intelmpi

ulimit -s unlimited

rm -f *.out

srun ../../../bin/hello_hybrid

