#!/bin/bash
#SBATCH -J mpi
#SBATCH --nodes=7
#SBATCH --ntasks=476
#SBATCH --ntasks-per-node=68
#SBATCH --cpus-per-task=1
#SBATCH --time=0:40:00
#SBATCH -C quad,cache
#SBATCH --exclusive
#SBATCH --mem=50GB
#SBATCH --output mpi.output.slurm 

set -e

export I_MPI_DOMAIN=auto
export I_MPI_PIN_RESPECT_CPUSET=0


module load intel intelmpi

ulimit -s unlimited

srun ../../../bin/hello_mpi


