#!/bin/bash
#SBATCH -J mpi_hello
#SBATCH --ntasks=48
#SBATCH --ntasks-per-node=24
#SBATCH --time=0:40:00

#SBATCH -C HSW24 

#SBATCH --exclusive
#SBATCH --output mpi.output.slurm.%J

set -e

module load intel openmpi/icc/2.0.2

ulimit -s unlimited

export MXM_LOG_LEVEL=error
srun ../../../bin/hello_mpi
