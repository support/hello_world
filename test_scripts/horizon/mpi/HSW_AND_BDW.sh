#!/bin/bash
#SBATCH -J mpi
#SBATCH --ntasks=176
#SBATCH --time=0:40:00

#EXPLICIT AND
#SBATCH -C [BDW28*8&HSW24*2]

#SBATCH --exclusive
#SBATCH --output mpi.output.slurm 

set -e

module load intel openmpi/icc/2.0.2

export MXM_LOG_LEVEL=error
srun ../../../bin/hello_mpi


